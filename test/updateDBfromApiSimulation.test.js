import {updateDBfromApiSimulation, apiCallSimulation} from './updateDBfromApiSimulation.js';
import {expect} from "chai"

describe("MERGE", () => {
    let testCases = [
        {
            description : "all deleted",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [],
            added : [],
            deleted : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ]
        },
        {
            description : "all deleted and some added",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11},
            ],
            added : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11}
            ],
            deleted : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ]
        },
        {
            description : "nothing changed",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            added : [],
            deleted : []
        },
        {
            description : "some deleted and some added",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11},
                { id: 2, time: 8},
                { id: 4, time: 6},
                { id: 6, time: 3}
            ],
            added : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11}
            ],
            deleted : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 3, time: 7},
                { id: 5, time: 5},
            ]
        },
        {
            description : "some deleted and some added (input apiArray part is empty)",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11},
                { id: 2, time: 8},
                { id: 4, time: 6},
                { id: 6, time: 3}
            ],
            added : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 3, time: 11}
            ],
            deleted : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 3, time: 7},
                { id: 5, time: 5},
            ]
        },
        {
            description : "some deleted",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 4, time: 6},
                { id: 5, time: 5},
            ],
            added : [],
            deleted : [
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 6, time: 3}
            ]
        },
        {
            description : "some added",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            added : [
                { id: 7, time: 14},
                { id: 8, time: 13},
                { id: 1, time: 12},
            ],
            deleted : []
        },
        {
            description : "some deleted and added again",
            dbArray : [
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 5, time: 5},
                { id: 6, time: 3}
            ],
            apiArray : [
                { id: 6, time: 15},
                { id: 3, time: 13},
                { id: 4, time: 12},
                { id: 0, time: 10},
                { id: 1, time: 9},
                { id: 2, time: 8},
                { id: 5, time: 5},
            ],
            added : [
                { id: 6, time: 15},
                { id: 3, time: 13},
                { id: 4, time: 12}
            ],
            deleted : [
                { id: 3, time: 7},
                { id: 4, time: 6},
                { id: 6, time: 3}
            ]
        },
    ]

    testCases.forEach(testCase => {
        console.log(testCase)
        describe(testCase.description, function () {
            console.log(testCase)
            let {
                merge,
                deleted,
                added
            } = updateDBfromApiSimulation(testCase.dbArray, apiCallSimulation(testCase.apiArray, 0, 2), testCase.apiArray)
            console.log("Merge: ", merge, "\nAdded:", added, "\nDeleted:", deleted)

            it("Merge", () =>
                expect(merge).eql(testCase.apiArray)
            )
            it("Added", () =>
                expect(added).eql(testCase.added)
            )
            it("Deleted", () =>
                expect(deleted).eql(testCase.deleted)
            )
        })
    });
})


export function apiCallSimulation(arr, offset, limit) {
    return arr.slice(offset, offset + limit);
}


export function updateDBfromApiSimulation(dbArray, apiArray, apiArrayFull) {
    if (apiArray.length === 0) {
        let nextNewArrPart = apiCallSimulation(apiArrayFull, apiArray.length, 2)
        apiArray = apiArray.concat(nextNewArrPart)
    }

    let deleted = [];
    let added = [];
    let merge = []

    let i = 0;
    let j = 0;
    let check = true;
    let dif = dbArray.length - apiArrayFull.length;

    while ((dif !== 0 || check) && j !== apiArray.length) {
        if (dbArray[i].time > apiArray[j].time) {
            deleted.push(dbArray[i]);
            dif--;
            i++;
        } else if (dbArray[i].time < apiArray[j].time) {
            merge.push(apiArray[j]);
            added.push(apiArray[j]);
            dif++;
            check = false;
            j++;
        } else if (dbArray[i].time === apiArray[j].time && dbArray[i].id === apiArray[j].id) {
            merge.push(dbArray[i])
            i++;
            j++;
        }

        if (j === apiArray.length) {
            let nextNewArrPart = apiCallSimulation(apiArrayFull, apiArray.length, 2)
            apiArray = apiArray.concat(nextNewArrPart)
        }
    }

    while (i !== dbArray.length) {
        if (j === apiArray.length) {
            deleted.push(dbArray[i])
        } else {
            merge.push(dbArray[i])
        }
        i++
    }

    return {merge, deleted, added}
}











const SpotifyWebApi = require('spotify-web-api-node');

class SpotifyAPI extends SpotifyWebApi {
    Outh (req, res) {
        const error = req.query.error;
        const code = req.query.code;
        const state = req.query.state;
    
        if (error) {
            console.error('Callback Error:', error);
            res.send(`Callback Error: ${error}`);
            return;
        }
    
        spotifyApi
        .authorizationCodeGrant(code)
        .then(data => {
            const access_token = data.body['access_token'];
            const refresh_token = data.body['refresh_token'];
            const expires_in = data.body['expires_in'];
    
            spotifyApi.setAccessToken(access_token);
            spotifyApi.setRefreshToken(refresh_token);
    
            console.log('access_token:', access_token);
            console.log('refresh_token:', refresh_token);
    
            console.log(
                `Sucessfully retreived access token. Expires in ${expires_in} s.`
            );
            res.send('Success! You can now close the window.');
    
            setInterval(async () => {
                const data = await spotifyApi.refreshAccessToken();
                const access_token = data.body['access_token'];
    
                console.log('The access token has been refreshed!');
                console.log('access_token:', access_token);
                spotifyApi.setAccessToken(access_token);
            }, expires_in / 2 * 1000);
        })
        .catch(error => {
            console.error('Error getting Tokens:', error);
            res.send(`Error getting Tokens: ${error}`);
        });
    }

    async getAllMySavedTracks () {
        let tracks = [];
        let cursor = 0
        let count = 0
        let next = false;
        do {
            let data = await this.getMySavedTracks({
                limit : 50,
                offset: cursor
            })

            tracks.push(data.body.items)
            next = !!(data.body.next);
            count++;
            cursor += 50;
            console.log(count, cursor, tracks.length)
            
         } while (next)

         console.log("STOP")
    }

    // function getSavedTracks(limit = 25000) {
    //     let requestCount = Math.ceil(limit / 50);
    //     let items = SpotifyRequest.getItemsByPath('me/tracks?limit=50', requestCount);
    //     return Selector.sliceFirst(extractTracks(items), limit);
    // }

    // function getItemsByPath(urlPath, limitRequestCount) {
    //     let url = `${API_BASE_URL}/${urlPath}`;
    //     let response = get(url);
    //     if (response.items.length < response.total) {
    //         let method = response.cursors ? getItemsByCursor : getItemsByNext;
    //         return method(response, limitRequestCount - 1);
    //     }
    //     return response.items;
    // }

    // function getItemsByCursor(response, limitRequestCount = 220) {
    //     let items = response.items;
    //     let count = 1;
    //     while (response.next != null && count != limitRequestCount) {
    //         response = get(response.next);
    //         Combiner.push(items, response.items);
    //         count++;
    //     }
    //     return items;
    // }

    scopes = [
        'ugc-image-upload',
        'user-read-playback-state',
        'user-modify-playback-state',
        'user-read-currently-playing',
        'streaming',
        'app-remote-control',
        'user-read-email',
        'user-read-private',
        'playlist-read-collaborative',
        'playlist-modify-public',
        'playlist-read-private',
        'playlist-modify-private',
        'user-library-modify',
        'user-library-read',
        'user-top-read',
        'user-read-playback-position',
        'user-read-recently-played',
        'user-follow-read',
        'user-follow-modify'
    ];
}

const spotifyApi = new SpotifyAPI({
    redirectUri: 'http://localhost:8888/callback/',
    clientId: 'f554f58afe3948d39a3ab26625c0774d',
    clientSecret: '8bed2522ce744c5ab25a5f3cd427093c'
});

  
spotifyApi.setAccessToken("BQB7Mc4DG3zxorZo8wr4IdXm3PDRue4_qBnUfTqvJI-sHlqn9kgKqpzaksbiQc2Oj6B2V2D2IxVn-n6Tq-bWQXRLU6NJBAGAg22UM5lWCrvqfxPLZylmK7T6lE8PNAzY5BDCTRFAwx8fu16u14zSceinzJQJWVdpttEUqAm4J6ro-IV93geUvxSzO1sRTE2Xc3QWCqii70oZ7LoGq1wf7tQfmKyX6jxTrsI-NXEYXJM5rYtU1r3OVpTqyZJ-8rKCcrGYU8_i-ZLoPNaxvUVTKAWWEIZ4qH5KrakdHxi7R_a0u1WjHXGXmPvjyI_ie7GqkZIP6ibbiaIABcWT")
spotifyApi.setRefreshToken("AQCz0cjFqpy8tMJs9LqjnPyKDR_Zhh9NoiGNKRmSInKUpLadJjfBc_l2Il4aDJaQ9TDKSzFI2-LV7HHbAvFzNl44EEeqlVIvX5WM8WYONEE397ZRUdEpgYbMRf-neN4TK4M");

module.exports = spotifyApi;
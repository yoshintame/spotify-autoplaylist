const EventEmmiter = require('events')
const spotifyApi = require('./spotifyApi')

class SpotifyEmitter extends EventEmmiter {
    constructor(options) {
        super()
        this.total = 5130
        this.lastAddTime = "2022-12-16T01:58:50Z"
        this.savedTracksUpdate()
    }

    savedTracksUpdate() {
        // console.log(this.total)
        setInterval(() => {
            // console.log(this.total)

            spotifyApi.getMySavedTracks({
                limit : 50,
                offset: 0
            })
            .then((data) => {
                // console.log(data.body.total, data.body.items[0].added_at )
                if (data.body.total != this.total || data.body.items[0].added_at != this.lastAddTime){
                    this.emit('saved_tracks_update', {total: this.total, lastAddTime: this.lastAddTime})
                    console.log("Emitted")
                    this.total = data.body.total    
                    this.lastAddTime = data.body.items[0].added_at 
                    return
                }
                // console.log("Not emitted")
        
            }, function(err) {
                console.log('Something went wrong!', err);
            });
        }, 1000);
    }
}

module.exports = SpotifyEmitter
const express = require('express');
const spotifyApi = require('./spotifyApi')
const SpotifyEmitter = require('./spotifyEmitter')
const spotifyEmitter = new  SpotifyEmitter();
const app = express();

app.get('/login', (req, res) => {
    res.redirect(spotifyApi.createAuthorizeURL(spotifyApi.scopes));
});

app.get('/callback', spotifyApi.Outh);


app.listen(process.env.PORT || 8888, (err) => {
    if (err) {
      return console.log(err);
    }
    
    console.log('HTTP Server up. Now go to http://localhost:8888/login in your browser.')
});

spotifyEmitter.on('saved_tracks_update', (args) => {
    const { total, last_added } = args;
    // let tracks = spotifyApi.getAllMySavedTracks();
    // console.log(total, last_added);
})







